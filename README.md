# web_service_project01

這次作業的專案名稱為hw_project01
而這個專案底下有主要幾個資料夾


* 1.book_api --> 負責設計與DB處理的api
  * (1) fields.py , models.py , serialize.py 負責與DB的建立與連結
  * (2) views.py 負責將讀取回來的資料做處理並設計兩個class物件 (代表兩隻不同的api)

* 2.index --> 負責呈現網站首頁的所有程式
  * (1) views.py 負責處理撈取template格式呈現於瀏覽器上

* 3.hw_project01 --> 所有專案的設定檔都在這裡 （包含資料庫設定檔、前端設定檔）
  * (1)settings.py 設定資料庫參數、template路徑、static路徑

* 4.static --> 負責存放js,css,scss的程式檔，來負責處理Ajax跟版型與美觀設計
  * (1)依據js,css,scss,image,vendor來進行分類

* 5.template --> 負責網站呈現的樣板與區塊
  * (1)設計html檔 (Base,header,index)