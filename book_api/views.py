# -*- coding: UTF-8 -*-

# views.py process REST_API include get data of all books and particular book.
from __future__ import unicode_literals
import os
import sys
import json

# import Django lib to process the procedure of Request.
from django.shortcuts import render,redirect
from django.views.decorators.csrf import csrf_exempt
from django.template import RequestContext
from django.template.loader import get_template

# import Django REST-Framework to process REST_API.
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status
from .serializers import BookSerializer
from .models import Book


# BookDetail class is reponsible to get data of particular book_ID and process Ajax.
class BookDetail(APIView):
    def get_object(self,pk):
        try:
            # get data by book_ID.
            return Book.objects.get(id__exact=pk)
        except Book.DoesNotExist as e:
            raise Http404

    # GET Method
    def get(self,request,pk,format=None):
        book = self.get_object(pk)
        serializer = BookSerializer(book)
        data = serializer.data

        # Ajax return data and its format.
        json_response_format = {
            'error':False,
            "message":"",
            "book":data
        }
        return Response(json_response_format)

    # PUT Method
    def put(self,request,pk,format=None):
        book = self.get_object(pk)
        serializer = BookSerializer(book,data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data,status=status.HTTP_200_OK)
        return Response(serializer.errors,status=status.HTTP_400_BAD_REQUEST)

    # DELETE Method
    def delete(self,request,pk,format=None):
        book = self.get_object(pk)
        book.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)

# BookList class is reponsible to get all data and process Ajax.
class BookList(APIView):
    # GET Method
    def get(self,request,format=None):
        try:
            # get all data
            serializer = BookSerializer(Book.objects.all(),many=True)
            data = serializer.data
        except Book.DoesNotExist as e:
            raise Http404

        # Ajax return data and its format.
        json_response_format = {
            'error':False,
            "message":"",
            "books":data
        }

        return Response(json_response_format,status=status.HTTP_200_OK)

    # POST Method
    def post(self,request=None,format=None):
        serializer = BookSerializer(request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data,status=status.HTTP_200_OK)

        return Response(serializer.errors,status=status.HTTP_400_BAD_REQUEST)
