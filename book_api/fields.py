# -*- coding: UTF-8 -*-
import os
import sys
from django.db import models
from django.db.models import fields
from django.core.exceptions import FieldError

# fields.py define specific column db-type in book_api directory.
class BigIntegerAutoField(fields.BigIntegerField):
    def db_type(self, connection):
        if 'mysql' in connection.__class__.__module__:
            return 'bigint(20) AUTO_INCREMENT'
        return super(BigIntegerAutoField, self).db_type(connection)

class YearField(models.Field):
    def db_type(self, connection):
        if connection.settings_dict['ENGINE'] == 'django.db.backends.mysql':
            return 'year'
        else:
            raise FieldError
