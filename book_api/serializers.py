# -*- coding: UTF-8 -*-
from rest_framework import serializers
from .models import Book

# serializers.py select specific attribute of DB in book_api directory.
class BookSerializer(serializers.ModelSerializer):
    class Meta:
        model = Book
        fields = ('id','title','author','isbn','publisher','publication_year')
