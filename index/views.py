# -*- coding: UTF-8 -*-
import os
import sys
from django.shortcuts import render
from django.http import HttpResponse
from django.http import JsonResponse
from django.views.decorators.csrf import csrf_exempt
from django.template import RequestContext
from django.template.loader import get_template


def web_index(request):
    web_index_template = get_template('index.html')
    web_index_html = web_index_template.render(locals()) # 將template轉成html

    return HttpResponse(web_index_html)
